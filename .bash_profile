export NVM_DIR="$HOME/.nvm"
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"  # This loads nvm
[ -s "$NVM_DIR/bash_completion" ] && \. "$NVM_DIR/bash_completion"  # This loads nvm bash_completion

[[ -s "$HOME/.rvm/scripts/rvm" ]] && source "$HOME/.rvm/scripts/rvm" # Load RVM into a shell session *as a function*

eval $(keychain --eval --agents ssh id_rsa id_rsa_metal)
eval $(keychain --eval --agents gpg 1F46B408)
export GPG_TTY=$(tty)

export PATH="$HOME/.cargo/bin:$PATH"
